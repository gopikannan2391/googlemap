import React, { Component } from "react";
import Map from "./Map";

class Home extends Component {
  render() {
    return (
      <div style={{ margin: "50px" }}>
        <div className='form-group'>
          <label htmlFor=''>First Name</label>
          <input type='text' name='first_name' className='form-control' />
        </div>
        <div className='form-group'>
          <label htmlFor=''>Last name</label>
          <input type='text' name='last_name' className='form-control' />
        </div>

        <Map
          google={this.props.google}
          center={{ lat: 9.171004165530432, lng: 76.93046861572265 }}
          height='300px'
          zoom={10}
        />
      </div>
    );
  }
}

export default Home;
